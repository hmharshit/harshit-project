import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity } from 'react-native';
import { Images, CommonStyles, Colors } from '../../styles'




export default class TobBar2 extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { navigation, renderTopIcons } = this.props
        return (
            <View style={{
                flex: 1, maxHeight: 80, justifyContent: 'center', flexDirection: 'row', backgroundColor: 'white',
                shadowColor: '#000000',
                shadowOffset: {
                    width: 0,
                    height: 9
                },
                shadowOpacity: 0.46,
                shadowRadius: 9.51,
                elevation: 10
            }}>

                <TouchableOpacity style={{
                    flex: 1, justifyContent: 'center', alignContent: 'flex-start',
                    alignItems: 'flex-start'
                }}
                    onPress={() => {
                        if (!renderTopIcons) this.props.navigation.navigate('Home')
                    }}
                >
                    <Image style={{ left: 20 }} source={Images.logo} />
                </TouchableOpacity>
                {!renderTopIcons && <View style={{ flex: 1, backgroundColor: '', flexDirection: 'row', justifyContent: 'flex-end', alignContent: 'center', alignItems: 'center' }} >
                    <TouchableOpacity style={{ backgroundColor: '', right: 10, width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }} onPress={() => { navigation.navigate('Setting', { animationEnabled: null }) }}>
                        <Image source={Images.setting_icon} />
                    </TouchableOpacity>

                    <TouchableOpacity style={{ right: 10, width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }} onPress={() => { navigation.navigate('Profile') }}>
                        <Image source={Images.user_icon} />

                    </TouchableOpacity>
                </View>}
            </View>
        )
    }
}




