import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default class ProfileTextInput extends Component {
    constructor(props) {
        super(props);
        this.props = {
            text: 'suersh'
        }
    }

    render() {
        const { extraStyle, placeHolder, text, phoneOptions, editable, secureTextEntry } = this.props

        return (
            <View style={[styles.container, extraStyle, { flexDirection: 'row', justifyContent: 'center' }]}>
                <View style={{ width: 100, marginRight: 15 }}>
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'row' }}>
                        <Text style={{ marginLeft: 0, marginBottom: 0, color: '#999999', fontSize: 15, fontFamily: 'Prompt-Regular' }}>{placeHolder}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <TextInput
                        secureTextEntry={secureTextEntry}
                        editable={editable || false}
                        placeholderTextColor='black'
                        value={text}
                        style={{
                            textAlign: 'right', color: "#131313",
                            fontFamily: "Prompt-Regular",
                            fontSize: 15,
                        }}
                        secureTextEntry={this.props.secureTextEntry}
                        onChangeText={text => { this.setState({ text: text }), this.props.onChangeText(text) }}
                        placeholder={this.props.placeholder}></TextInput>
                </View>


            </View>
        )
    }
}

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        maxHeight: 55,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        borderBottomWidth: 1,
        borderBottomColor: Colors.UNDERLINE,
    },
    textInput: {
        fontFamily: Typography.FONT_FAMILY_REGULAR,
        color: Colors.TEXT_PLACEHOLDER,
        fontSize: Typography.FONT_SIZE_16
    }
});