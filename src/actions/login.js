import {
    SET_PHONE_LOGIN_VISIBLE,
    SET_USER_ID
} from "./types"


export function setPhoneLoginVisible(visible) {
    return {
        type: SET_PHONE_LOGIN_VISIBLE,
        visible
    };
}


export function setUserId(user_id) {
    return {
        type: SET_USER_ID,
        user_id
    }
}