import React, { Component } from 'react';
import { SafeAreaView, View, Button, Text, TouchableOpacity, StyleSheet, Dimensions, Alert } from 'react-native';
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import Loader from '../../components/molecules/Loader'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import TobBar1 from '../../components/atoms/TobBar1'
import ZingTextInput from '../../components/molecules/ZingTextInput'
import { Dropdown } from 'react-native-material-dropdown'
import {
  Notification
} from "../../components/util"

import {
  CheckSubscription,
  ScheduleNewMeeting
} from "../../services/meeting_services"

import { FetchTimezoneList } from "../../services/general_services"
import Clipboard from '@react-native-community/clipboard'
import { MONTH_MAPPING, R_MONTH_MAPPING } from "../../services/constants";
import moment from "moment";
import DropDownModal from "../../components/organisms/DropDownModal"
import { connect } from "react-redux"
import DateTimePicker from '@react-native-community/datetimepicker';


const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

const getCircularReplacer = () => {
  const seen = new WeakSet();
  return (key, value) => {
  if (typeof value === "object" && value !== null) {
      if (seen.has(value)) {
          return;
      }
      seen.add(value);
  }
  return value;
  };
};
class ScheduleScreen extends Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true

    this.state = {
      phone: '',
      password: '',
      IsLoader: false,
      refundSwitch: true,
      donationSwitch: true,
      cancelSwitch: false,
      reportSwitch: false,
      meetingTitle: '',
      description: '',
      duration_hour: [
        { Value: '00', Name: '0 Hour', Id: 1 },
        { Value: '01', Name: '1 Hour', Id: 2 },
        { Value: '02', Name: '2 Hours', Id: 3 },
        { Value: '03', Name: '3 Hours', Id: 4 },
        { Value: '04', Name: '4 Hours', Id: 5 },
        { Value: '05', Name: '5 Hours', Id: 6 },
        { Value: '06', Name: '6 Hours', Id: 7 },
        { Value: '07', Name: '7 Hours', Id: 8 },
        { Value: '08', Name: '8 Hours', Id: 9 },
        { Value: '09', Name: '9 Hours', Id: 10 },
        { Value: '10', Name: '10 Hours', Id: 11 },
        { Value: '11', Name: '11 Hours', Id: 12 },
        { Value: '12', Name: '12 Hours', Id: 13 },
        { Value: '13', Name: '13 Hours', Id: 14 },
        { Value: '14', Name: '14 Hours', Id: 15 },
        { Value: '15', Name: '15 Hours', Id: 16 },
        { Value: '16', Name: '16 Hours', Id: 17 },
        { Value: '17', Name: '17 Hours', Id: 18 },
        { Value: '18', Name: '18 Hours', Id: 19 },
        { Value: '19', Name: '19 Hours', Id: 20 },
        { Value: '20', Name: '20 Hours', Id: 21 },
        { Value: '21', Name: '21 Hours', Id: 22 },
        { Value: '22', Name: '22 Hours', Id: 23 },
        { Value: '23', Name: '23 Hours', Id: 24 },
      ],
      duration_minute: [
        { Value: '00', Name: '00 mins', Id: 1 },
        { Value: '05', Name: '05 mins', Id: 2 },
        { Value: '10', Name: '10 mins', Id: 3 },
        { Value: '15', Name: '15 mins', Id: 4 },
        { Value: '20', Name: '20 mins', Id: 5 },
        { Value: '25', Name: '25 mins', Id: 6 },
        { Value: '30', Name: '30 mins', Id: 7 },
        { Value: '35', Name: '35 mins', Id: 8 },
        { Value: '40', Name: '40 mins', Id: 9 },
        { Value: '45', Name: '45 mins', Id: 10 },
        { Value: '50', Name: '50 mins', Id: 11 },
        { Value: '55', Name: '55 mins', Id: 12 }
      ],
      timezones: [
      ],
    }
    this.scheduleMeeting = this.scheduleMeeting.bind(this)
  }


  copyToClipboard = (text) => {
    Clipboard.setString(text)
    Notification("Copied!", "success", 0)
  }


  scheduleMeeting() {

    const {
      title,
      selected_timezone,
      selected_day,
      selected_month,
      selected_year,
      selected_hour,
      selected_minute,
      selected_meredium,
      selected_duration_hour,
      selected_duration_minute,
      description
    } = this.state

    if (title && /[^a-zA-Z0-9_ \-\/]/.test(title)) {
      Notification("Meeting title should be alpha numeric", "error", 0)
      return false
    }

    if (!title || !selected_timezone.Value || !selected_year || !selected_day ||
      !selected_duration_hour.Value || !selected_hour || !selected_meredium || !selected_month
      || !selected_minute || !selected_duration_minute.Value) {

      Notification("All fields are mandatory", "warning", 0)

      return false
    }

    if (!this.validatePastTime()) {
      Notification("Past time is not allowed.", "warning")
      return false
    }

    this.setState({
      IsLoader: true
    })

    CheckSubscription(this.props.user_id).then(response => {

      if (response && !response.error && response.status === 200) {
        ScheduleNewMeeting(
          title,
          selected_timezone.Value,
          `${selected_year}-${selected_month}-${selected_day}`,
          `${selected_hour}:${selected_minute} ${selected_meredium}`,
          selected_duration_hour.Value,
          selected_duration_minute.Value,
          "1",
          Math.random().toString(36).substring(4, 12),
          "1",
          "1",
          "1",
          this.props.user_id,
          description
        ).then(response => {

          if (response && !response.error && response.status === 200) {
            Notification("Meeting scheduled successfully!", "success", 0)
            this.props.navigation.navigate('Home')
          }

          this.setState({ IsLoader: false })
        })

      }


      else if (response && response.error && response.status === 500) {
        Notification(response.result.message, "error", 1)

        this.setState({
          IsLoader: false
        })

      }
    })
  }

  getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value)
  }

  onChange(event, selected) {
    console.log(event, selected)

    if(event.type == "dismissed") {
      console.log("dis")
      return false
    }

    const { mode } = this.state


    if (mode === "date") {
      let date = moment(selected)
      this.setState({
        selected_day: date.format('DD'),
        selected_month: date.format('MM'),
        selected_year: date.format('YYYY'),
        selected_month_def: date.format('MMMM'),
        showDateTimePicker: false,
        timestamp: event.nativeEvent.timestamp
      })
    }

    else if (mode === "time") {
      let time = new Date(event.nativeEvent.timestamp).toLocaleTimeString().split(' ')
      console.log(time)
      let selected_hour, selected_minute, selected_meredium

        selected_hour = time[0].split(':')[0]
        selected_minute = time[0].split(':')[1]
        selected_meredium = time[1]


      if (selected_meredium == undefined || selected_meredium == "") {
        // special handling for specific ios devices
        if (parseInt(selected_hour) <= 12) {
          selected_meredium = "AM"
        }
        else {
          selected_hour = `${parseInt(selected_hour - 12)}`
          selected_meredium = "PM"
        }
      }

      console.log(selected_hour, selected_meredium, selected_minute)
      this.setState({
        selected_hour: selected_hour,
        selected_minute: selected_minute,
        selected_meredium: selected_meredium,
        showDateTimePicker: false,
        timestamp: event.nativeEvent.timestamp
      })
    }
  }
 
    validatePastTime() {
        let {
          selected_hour,
          selected_minute,
          selected_meredium,
          selected_day,
          selected_month,
          selected_year,
        } = this.state

        let current_time = new Date()
    
        if (selected_meredium.toLowerCase() == "pm" & selected_hour !=12) {
          selected_hour = `${parseInt(selected_hour) + 12}`
        }
        let d = `${selected_year}-${selected_month}-${selected_day} ${selected_hour}:${selected_minute}:00`

      //   Alert.alert(`Debug`, `${selected_hour},
      //   ---${selected_minute},
      //   ---${selected_meredium},
      //  ---${selected_day},
      //   ---${selected_month},
      //   ---${selected_year},
      //   ---${d} --- ${moment(d)} 
      //   ---> ${current_time}
      //      <---> ${moment(current_time)}`)

        console.log("###",
        moment(d, ["YYYY-MM-DD kk:mm:ss"]), 
        moment(current_time))
    
        if (moment(d, ["YYYY-MM-DD kk:mm:ss"]) < moment(current_time)) return false
        return true
    }

  componentDidMount() {

    let date = new Date();

    let month = date.getMonth() + 1

    if (month <= 9) {
      month = `0${month}`
    }
    else {
      month = `${month}`
    }

    let hour = date.getHours(), m;
    if (hour >= 12) {
      hour = hour == 12 ? hour : hour - 12
      m = "PM"
    }
    else {
      m = "AM"
    }

    this.setState({
      selected_day: date.getDate().toString(),
      selected_month_def: R_MONTH_MAPPING[date.getMonth()],
      selected_month: month,
      selected_year: date.getFullYear().toString(),
      selected_hour: hour.toString(),
      selected_minute: date.getMinutes() < 9 ? `0${date.getMinutes().toString()}` : date.getMinutes().toString(),
      selected_meredium: m,
      selected_duration_hour: {
        Name: "1 Hour", 
        Value: "1"
      },
      selected_duration_minute: {
        Name: "0 mins",
        Value: "0"
      }
    })
    FetchTimezoneList().then(response => {

      if (response && !response.error) {
        response = response.result;

        if (response && response.message === "timezone list") {
          let timezones = [], timezone_mapping = {};

          response.timezone.forEach((tz, i) => {
            timezones.push({
              Value: tz.timezone,
              Name: tz.utctime,
              Id: i + 1
            })

            timezone_mapping[tz.utctime] = tz.timezone
          })

          // console.log(timezone_mapping)

          this.setState({
            timezones: timezones,
            timezone_mapping: timezone_mapping,
            selected_timezone: {
              Name: this.getKeyByValue(timezone_mapping, this.props.user_timezone),
              Value: this.props.user_timezone
            }
          })
        }
      }
    })
  }

  render() {

    console.log(this.state.selected_day, this.state.selected_month)

    const { day, hour, minutes, duration_hour, duration_minute, timezones, code5, month, year } = this.state
    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <TobBar1 navigation={this.props.navigation}></TobBar1>
        <View style={styles.header}>
          <Text style={{ color: '#8BC341', fontFamily: 'Prompt-Regular', fontSize: 14 }}>SCHEDULE MEETING</Text>
          {/* <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.pop()}>
            <Image source={Images.back_icon} />
          </TouchableOpacity> */}
        </View>
        <KeyboardAwareScrollView style={{ flex: 1, width: '100%' }} contentContainerStyle={{ flexGrow: 1 }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', width: DeviceWidth / 100 * 90, justifyContent: 'center', marginLeft: 18, marginTop: 20 }}>
            <ZingTextInput
               extraStyle = {{height:64, borderBottomWidth:1}}
               maxLength={24}
              placeholder={'Meeting Title'}
              onChangeText={(text) => this.setState({ title: text })}
            />
          </View>

          {/* Dd1 */}
          <TouchableOpacity style={{ flexDirection: 'row', height: DeviceHeight / 100 * 10, marginTop: 20 }}
          onPress={() => this.setState({mode: 'date'}, () => this.setState({showDateTimePicker: true}))}
          >
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 15, marginLeft: 18, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1 }}>
            <Text
            style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center',marginTop: 12 }}
        >{this.state.selected_day}</Text>

            </View>
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 40, marginLeft: 15, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1 }}>
             
            <Text
            style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center',marginTop: 12 }}
              >{this.state.selected_month_def}</Text>
  
            </View>
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 25, marginLeft: 18, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1 }}>
            <Text
            style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center',marginTop: 12 }}
            >{this.state.selected_year}</Text>
       
             
            </View>
          </TouchableOpacity>

          {/* Dd2 */}
          <TouchableOpacity style={{ flexDirection: 'row', height: DeviceHeight / 100 * 10, marginTop: 10 }} 
          onPress={() => this.setState({mode: 'time'}, () => this.setState({showDateTimePicker: true}))}
          >
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 33, marginLeft: 18, alignItems: 'flex-start', justifyContent: 'center' }}>
              <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 15 }}>Time</Text>
            </View>
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 15, marginLeft: 13, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1 }}>
            <Text
            style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center',marginTop: 12 }}
            >{this.state.selected_hour}</Text>
         
              {/* <Dropdown
                placeholder='HH'
                placeholderTextColor={'#131313'}
                label=''
                dropdownPosition={0}
                labelHeight={7}
                style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center' }}
                affixTextStyle={styles.font}
                labelTextStyle={styles.font}
                value={this.state.selected_hour}
                titleTextStyle={styles.font}
                itemTextStyle={{ fontSize: 10, fontFamily: 'Prompt-Regular' }}
                pickerStyle={{ marginLeft: 20, width: 70 }}
                inputContainerStyle={{ marginLeft: 10, borderBottomColor: 'transparent' }}
                data={hour}
                valueExtractor={({ value }) => value}
                onChangeText={(e) => this.setState({
                  selected_hour: e
                })}
              /> */}
            </View>
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 15, marginLeft: 18, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1 }}>
            <Text
            style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center',marginTop: 12 }}
            >{this.state.selected_minute}</Text>
         
              {/* <Dropdown
                placeholder='MM'
                placeholderTextColor={'#131313'}
                label=''
                dropdownPosition={0}
                labelHeight={7}
                style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center' }}
                affixTextStyle={styles.font}
                labelTextStyle={styles.font}
                value={this.state.selected_minute}
                titleTextStyle={styles.font}
                itemTextStyle={{ fontSize: 10, fontFamily: 'Prompt-Regular' }}
                pickerStyle={{ marginLeft: 20, width: 70 }}
                inputContainerStyle={{ marginLeft: 10, borderBottomColor: 'transparent' }}
                data={minutes}
                valueExtractor={({ value }) => value}
                onChangeText={(e) => this.setState({
                  selected_minute: e
                })}
              /> */}
            </View>
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 15, marginLeft: 15, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1 }}>
             
            <Text
            style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center',marginTop: 12 }}
            >{this.state.selected_meredium}</Text>
              {/* <Dropdown
                placeholder='PM'
                placeholderTextColor={'#131313'}
                label=''
                dropdownPosition={0}
                labelHeight={7}
                style={{ fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center' }}
                affixTextStyle={styles.font}
                labelTextStyle={styles.font}
                value={this.state.selected_meredium}
                titleTextStyle={styles.font}
                itemTextStyle={{ fontSize: 10, fontFamily: 'Prompt-Regular' }}
                pickerStyle={{ marginLeft: 20, width: 70 }}
                inputContainerStyle={{ marginLeft: 5, borderBottomColor: 'transparent' }}
                data={code5}
                valueExtractor={({ value }) => value}
                onChangeText={(e) => this.setState({
                  selected_meredium: e
                })}
              /> */}
            </View>
          </TouchableOpacity>

          {/* Dd3 */}
          <View style={{ flexDirection: 'row', height: DeviceHeight / 100 * 10, marginTop: 10 }}>
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 33, marginLeft: 18, alignItems: 'flex-start', justifyContent: 'center' }}>
              <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 15 }}>Duration</Text>
            </View>
            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 23, marginLeft: 13, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1 }}>
     
            <DropDownModal
              data={duration_hour}
                onClose={() => null}
                hideIndex={true}
                selected={this.state.selected_duration_hour}
                onSelect={(selected) => Object.keys(selected).length > 0 && this.setState({selected_duration_hour: selected})}
                onBackButton={() => null}
              render={(disabled, selected, showModal) => 
              
          <TouchableOpacity style={{ marginTop: 12 }} onPress={showModal}>

<Text
style={{fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center'}}
              >{this.state.selected_duration_hour && this.state.selected_duration_hour.Name}</Text></TouchableOpacity>}
            />
            

            </View>

            <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 25, marginLeft: 20, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1 }}>
         
            <DropDownModal
              data={duration_minute}
                onClose={() => null}
                hideIndex={true}
                selected={this.state.selected_duration_minute}
                onSelect={(selected) => Object.keys(selected).length > 0 && this.setState({selected_duration_minute: selected})}
                onBackButton={() => null}
              render={(disabled, selected, showModal) => 
              
          <TouchableOpacity style={{ marginTop: 12 }} onPress={showModal}>

<Text
style={{fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center'}}
              >{this.state.selected_duration_minute && this.state.selected_duration_minute.Name}</Text></TouchableOpacity>}
            />
            
            </View>
          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', width: DeviceWidth / 100 * 90, justifyContent: 'center', marginBottom: 7, marginLeft: 18, marginTop: 0 }}>
            <ZingTextInput
               extraStyle = {{height:64, borderBottomWidth:1}}
               placeholder={'Description'}
              onChangeText={(text) => this.setState({ description: text })}
            />
          </View>

          <View
              style={{
                flexDirection: "column",
                marginTop: 15,
                height: DeviceHeight / 100 * 7,
                marginLeft: 13,
                borderBottomColor: Colors.UNDERLINE,
                borderBottomWidth: 1,
              }}
            >
          <DropDownModal
            data={timezones}
            onClose={() => null}
            selected={this.state.selected_timezone}
            onSelect={(selected) => Object.keys(selected).length > 0 && this.setState({ selected_timezone: selected })}
            onBackButton={() => null}
            render={(disabled, selected, showModal) =>

              <TouchableOpacity style={{ flexDirection: 'row', height: DeviceHeight / 100 * 10, marginTop: 10, justifyContent: "center" }} onPress={showModal}>
                  <Text
        style={{fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center'}}
                  >{this.state.selected_timezone && this.state.selected_timezone.Name}
                </Text></TouchableOpacity>}
          /></View>
            

   


          {/* <View style={{ flex: 1, flexDirection: 'row', marginLeft: 18, marginBottom: 10 }}>
            <View style={{ flexDirection: 'column', width: '50%', marginLeft: 18, alignItems: 'flex-start', justifyContent: 'center' }}>
              <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 15, marginTop: 3 }}>Invitation Link</Text>
            </View>
            <View style={{ flexDirection: 'column', width: '50%' }}>
              <View style={{ flex: 1, flexDirection: 'row' }}
              >
                <View style={{ flexDirection: 'column', width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                  <TouchableOpacity 
                                onPress={() => this.copyToClipboard("https://xd.adobe.com/view/4c0b7275-e122-4da8-6f2d-3c2b26c90890-3a3a/screen/1e90ce8e-3fbc-4e51-a8af-a18d538be42e/03-Sign-In-mobile")}

                  >
                    <Image source={Images.copy} style={{ marginTop: 6 }} />
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'column', width: '80%', alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#8BC341', fontFamily: 'Prompt-Regular', fontSize: 15, marginTop: 3 }}>Copy Invitation</Text>
                </View>
              </View>
            </View>
          </View> */}

          {/* <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1, width: '90%', marginLeft: 18, marginBottom: 10 }}>
            <Text style={{ color: '#1492E6', fontFamily: 'Prompt-Regular', fontSize: 15, marginTop: 5 }}>
              https://xd.adobe.com/view/4c0b7275-e122-4da8-6f2d-3c2b26c90890-3a3a/screen/1e90ce8e-3fbc-4e51-a8af-a18d538be42e/03-Sign-In-mobile
                    </Text>
          </View> */}

          {/* <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', marginBottom: 10 }}>
            <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 15, marginTop: 8, marginLeft: 18 }}>
              Add to
                    </Text>
          </View> */}

          {/* 
          <View style={{ flexDirection: 'row', height: '13%', marginTop: 10 }}>
            <View style={{ flexDirection: 'row', width: '50%', height: '65%', borderRadius: 10, margin: 9, borderWidth: 1, borderColor: '#8BC341' }}>
              <View style={{ flexDirection: 'column', width: '20%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity >
                  <Image source={Images.agenda} />
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'column', width: '80%', justifyContent: 'center' }}>
                <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 15 }}>Google Calendar</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', width: '40%', height: '65%', borderRadius: 10, margin: 9, borderWidth: 1, borderColor: '#8BC341' }}>
              <View style={{ flexDirection: 'column', width: '20%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity >
                  <Image source={Images.addtoicall} />
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'column', width: '80%', justifyContent: 'center' }}>
                <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 15 }}>ical</Text>
              </View>
            </View>
          </View> */}

          <View style={{ flexDirection: 'row', height: '13%', marginBottom: 5 }}>
            {/* <View style={{ flexDirection: 'row', width: '50%', height: '65%', borderRadius: 10, margin: 9, borderWidth: 1, borderColor: '#8BC341' }}> */}
            {/* <View style={{ flexDirection: 'column', width: '20%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity >
                  <Image source={Images.addtooutlook} />
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'column', width: '80%', justifyContent: 'center' }}>
                <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 15 }}>Outlook Calendar</Text>
              </View> */}
            {/* </View> */}

          </View>

        </KeyboardAwareScrollView>


        <TouchableOpacity style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView, { marginTop: 10, marginBottom: 30 }]} onPress={this.scheduleMeeting}>
          <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Schedule A Meeting</Text>
        </TouchableOpacity>
        {/* 
        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: 30 }}>
          <Text style={{ color: '#8BC341', fontFamily: 'Prompt-Medium', fontSize: 15 }}>DELETE MEETING</Text>
        </TouchableOpacity> */}

      {this.state.showDateTimePicker && (
        <DateTimePicker
          testID="dateTimePicker"
          value={this.state.timestamp ? new Date(this.state.timestamp) : new Date()}
          minimumDate={new Date()}
          mode={this.state.mode}
          is24Hour={true}
          display="default"
          onChange={(event, selected) => this.onChange(event, selected)}
        />
      )}  
        <Loader setModalVisible={this.state.IsLoader} ></Loader>

      </SafeAreaView >
    )
  }
}

const _mapStateToProps = (state) => {
  return {
    user_id: state['home'].user_data.id,
    user_timezone: state['home'].user_data.timezone || null
  }
}


export default connect(_mapStateToProps)(ScheduleScreen)


export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    textAlign: 'center',
    width: 330,
    height: 100,
    marginBottom: 10,
    lineHeight: 25,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16
  },
  backIcon: {
    left: 20,
    width: 40,
    height: 40,
    position: 'absolute',
  },
  font: {
    fontFamily: 'Prompt-Regular',
  },
  header: {
    flex: 1,
    maxHeight: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
});