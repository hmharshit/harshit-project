import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TextInput, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import Loader from '../../components/molecules/Loader'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import TopBar from '../../components/atoms/TopBarWithoutProfile'
import ZingTextInput from '../../components/molecules/ZingTextInput'
import ProfileTextInput from '../../components/molecules/ProfileTextInput'
import { Dropdown } from 'react-native-material-dropdown'
// import { BottomSheet } from 'react-native-btr';

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

export default class ProfileEditScreen extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true

        this.state = {
            name: '',
            visible: false,
            account: '',
            password: '',
            IsEditOn: false,
            IsLoader: false,
            IsPasswordShow: false,
            code8: [
                { value: '(GMT +5:30)India' },
                { value: '(GMT +6:00)Usa' },
            ],
        }

        this.onSigninDidSelected = this.onSigninDidSelected.bind(this)
    }

    onStateUpdated(data) {
        const { phone, password } = { data }
        this.setState({ phone: data.phone, password: data.password })
        console.log(this.state);
    }

    onSigninDidSelected() {
        console.log('suresh');
        console.log(this.state);
        this.setState({
            IsLoader: true
        })

        setTimeout(
            function () {
                this.setState({ IsLoader: false });
                this.props.navigation.navigate('Home')
            }
                .bind(this),
            1000
        );
    }

    onSigninDidSelected = () => {
        this.props.navigation.navigate('Profile')
    }

    _toggleBottomNavigationView = () => {
        this.setState({ visible: !this.state.visible });
    }



    render() {
        const { code8 } = this.state

        return (
            <SafeAreaView style={CommonStyles.ZView.container}>
                <TopBar navigation={this.props.navigation}></TopBar>
                <KeyboardAwareScrollView style={{ flex: 1, width: DeviceWidth }} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={[CommonStyles.ZView.container, {}]}>
                        <View style={styles.header}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }} >
                                    <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.pop()}>
                                        <Image source={Images.back_icon} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ alignItems: 'center', flex: 5 }} >
                                    <View style={{ width: 120, height: 120 }}>
                                        <Image style={{ width: 120, height: 120, borderRadius: 15 }} source={Images.profileImage} />
                                        <TouchableOpacity style={styles.cameraIcon}>
                                            <Image source={Images.camera_icon} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <View style={{ flex: 0.70, flexDirection: 'column' }}>
                                <ProfileTextInput
                                    placeHolder={'Enter the Email'}
                                    text={this.state.account}
                                    onChangeText={(text) => { this.setState({ account: text }) }}>
                                </ProfileTextInput>
                                <ProfileTextInput
                                    placeHolder={'Enter the  Name'}
                                    text={this.state.name}
                                    onChangeText={(text) => { this.setState({ name: text }) }}>
                                </ProfileTextInput>
                                <ProfileTextInput
                                    placeHolder={'Enter the Password'}
                                    text={this.state.pas}
                                    onChangeText={(text) => { this.setState({ password: text }) }}>
                                </ProfileTextInput>


                                <View style={{ flexDirection: 'row' }} >
                                    <TouchableOpacity
                                        style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', right: 30, top: 15 }}
                                        onPress={this._toggleBottomNavigationView}>
                                        <Text style={{ color: Colors.PRIMARY, fontSize: Typography.FONT_SIZE_14 }}>CHANGE</Text>
                                    </TouchableOpacity>
                                </View>

                                {/* Time */}
                                <View style={{ flexDirection: 'row', height: '17%', top: 15 }}>
                                    <View style={{
                                        flexDirection: 'column', height: '85%', width: '90%', marginLeft: 20, borderBottomColor: Colors.UNDERLINE,
                                        borderBottomWidth: 1,
                                    }}>
                                        <Dropdown
                                            placeholder='Select Time'
                                            placeholderTextColor={'black'}
                                            label=''
                                            dropdownPosition={0}
                                            labelHeight={7}
                                            style={{ fontSize: 15, fontFamily: 'Prompt-Regular', marginLeft: 100 }}
                                            affixTextStyle={{ fontFamily: 'Prompt-Regular' }}
                                            labelTextStyle={{ fontFamily: 'Prompt-Regular' }}
                                            titleTextStyle={{ fontFamily: 'Prompt-Regular' }}
                                            itemTextStyle={{ fontSize: 10, fontFamily: 'Prompt-Regular' }}
                                            pickerStyle={{ marginLeft: 70, width: 270 }}
                                            inputContainerStyle={{ marginRight: 40, borderBottomColor: 'transparent' }}
                                            data={code8}
                                            valueExtractor={({ value }) => value}
                                        />
                                    </View>
                                </View>
                            </View>

                            <View style={{ flex: 0.30, flexDirection: 'column', bottom: 40 }}>
                                <TouchableOpacity style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView, { margin: 80, marginBottom: 150 }]}
                                    onPress={this.onSigninDidSelected}>
                                    <Text style={{ color: 'white' }}>Update</Text>
                                </TouchableOpacity>
                            </View>

                            {/* {this.state.IsEditOn ? <TouchableOpacity style={{ flex: 1, alignSelf: 'flex-end', width: 120, margin: 10, marginRight: 20, maxHeight: 40, justifyContent: 'center', alignItems: 'flex-end' }} onPress={() => { this.setState({ IsPasswordShow: true }) }} >
                                <Text style={{ color: Colors.PRIMARY, fontSize: Typography.FONT_SIZE_16 }}>CHANGE</Text>
                            </TouchableOpacity> : null} */}

                            {/* {this.state.IsEditOn ? (<TouchableOpacity style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView, { marginLeft: 20, marginRight: 20, marginTop: 50 }]} onPress={() => { this.setState({ IsEditOn: false }) }}>
                                <Text style={{ color: 'white', fontSize: 18 }}>Update</Text>
                            </TouchableOpacity>) : (<TouchableOpacity style={{ flex: 1, maxHeight: 50, marginTop: 150, justifyContent: 'flex-start', alignItems: 'center' }} onPress={() => { this.props.navigation.navigate('Auth') }} >
                                <Text style={{ color: Colors.PRIMARY, fontSize: Typography.FONT_SIZE_16 }}>SIGN OUT</Text>
                            </TouchableOpacity>)} */}
                        </View>
                    </View>
                </KeyboardAwareScrollView>

                <Loader setModalVisible={this.state.IsLoader} ></Loader>

                {/* Menu PopUp */}
                {/* <BottomSheet
                    visible={this.state.visible}
                    onBackButtonPress={this._toggleBottomNavigationView}
                    onBackdropPress={this._toggleBottomNavigationView}
                >
                    <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{
                            borderRadius: 10, backgroundColor: 'white', width: '90%', height: '55%',

                        }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 25 }}>
                                <Text style={{ textAlign: 'center', color: Colors.TEXT_PLACEHOLDER, fontSize: Typography.FONT_SIZE_16 }}>{"Please enter the old password and  \n new password to update"}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <ZingTextInput
                                    extraStyle={{ marginBottom: 10, marginTop: 10 }}
                                    phoneOptions={false}
                                    secureTextEntry={false}
                                    placeholder={"Old Password"}
                                    onChangeText={(text) => { this.setState({ phone: text }) }} >
                                </ZingTextInput>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <ZingTextInput
                                    extraStyle={{ marginBottom: 10, marginTop: 10 }}
                                    phoneOptions={false}
                                    secureTextEntry={false}
                                    placeholder={"New Password"}
                                    onChangeText={(text) => { this.setState({ phone: text }) }}>
                                </ZingTextInput>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <ZingTextInput
                                    extraStyle={{ marginBottom: 10, marginTop: 10 }}
                                    phoneOptions={false}
                                    secureTextEntry={false}
                                    placeholder={"Confirm Password"}
                                    onChangeText={(text) => { this.setState({ phone: text }) }} >
                                </ZingTextInput>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                                <TouchableOpacity
                                    style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView, { marginTop: 40 }]}
                                    onPress={this.onSigninDidSelected}>
                                    <Text style={{ color: 'white' }}>Update</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </BottomSheet> */}

            </SafeAreaView >
        )
    }
}


export const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        textAlign: 'center',
        width: 330,
        height: 100,
        marginBottom: 10,
        lineHeight: 25,
        color: Colors.TEXT_PLACEHOLDER,
        fontSize: Typography.FONT_SIZE_16
    },
    header: {
        flex: 1,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        maxHeight: 150,
        alignItems: 'center',
        justifyContent: 'center',
    },
    backIcon: {
        width: 40,
        height: 40,
        position: 'absolute',
    },
    cameraIcon: {
        right: -20,
        bottom: -20,
        width: 40,
        height: 40,
        position: 'absolute'
    },

    container: {
        flex: 1,
        maxHeight: 55,
        marginLeft: 20,
        marginRight: 20,
        borderBottomWidth: 1.5,
        borderBottomColor: Colors.UNDERLINE,
    },
    textInput: {
        fontFamily: Typography.FONT_FAMILY_REGULAR,
        color: Colors.TEXT_PLACEHOLDER,
        fontSize: Typography.FONT_SIZE_16
    }
});