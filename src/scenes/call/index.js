import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, FlatList, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import CallMenu from '../../components/molecules/CallMenu'
import { BottomSheet } from 'react-native-btr';


const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'A',
    audio: false,
    video: false,
    hoster: true,
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'A',
    audio: false,
    video: false,
    hoster: true,
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'A',
    audio: false,
    video: false,
    hoster: true,
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'A',
    audio: false,
    video: false,
    hoster: true,
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'A',
    audio: false,
    video: false,
    hoster: true,
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'A',
    audio: false,
    video: false,
    hoster: true,
    visible: false
  }
];


export default class CallScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      phone: '',
      password: '',
      IsLoader: false,
      showMenu: false,
    }
    this.onSigninDidSelected = this.onSigninDidSelected.bind(this)
  }

  onStateUpdated(data) {
    const { phone, password } = { data }
    this.setState({ phone: data.phone, password: data.password })
    console.log(this.state);
  }

  onSigninDidSelected() {
    console.log('suresh');
    console.log(this.state);
    this.setState({ IsLoader: true, IsPopupShow: false })

    setTimeout(
      function () {
        this.setState({ IsLoader: false });
        this.props.navigation.navigate('Call')
      }.bind(this),
      1000
    );
  }


  _renderItem = ({ item, index }) => { // Passing as object from here
    return (
      <View style={{ width: 100, height: 100, justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ width: 90, height: 85, borderColor: Colors.BLUE, borderWidth: 3, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>


          {this.state.IsPopupShow ? (<View style={{ width: 40, height: 40, backgroundColor: Colors.PRIMARY, justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}>
            <Text style={{ color: 'white', fontSize: 22 }}>{item.name}</Text>
          </View>) : (<View style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Image style={{ resizeMode: 'cover', width: 80, height: 80 }} source={Images.profileImage}></Image>
          </View>)}

          <View style={{ flex: 1, width: '100%', height: '100%', position: 'absolute', flexDirection: 'row' }}>


            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
              <Image style={{ margin: 2, width: 18, height: 18 }} source={Images.audiocall_speaker_icon}></Image>
              <Image style={{ margin: 2, width: 18, height: 18 }} source={Images.audiocall_admin_icon}></Image>
            </View>

            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
              <Image style={{ margin: 2, width: 18, height: 18 }} source={Images.audiocall_mute_icon}></Image>
              <Image style={{ margin: 2, width: 18, height: 18 }} source={Images.videocall_mute_icon}></Image>
            </View>
          </View>
        </View>
      </View>
    )
  }


  _toggleBottomNavigationView = () => {
    this.setState({ visible: !this.state.visible });
  }


  render() {
    return (
      <SafeAreaView style={[CommonStyles.ZView.container]}>
        <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center', backgroundColor: this.state.IsPopupShow ? 'black' : 'white' }}>
          {this.state.IsPopupShow == true ? <Image style={{ flex: 1, bottom: 50, maxWidth: 230, maxHeight: 280, resizeMode: 'cover', borderRadius: 15 }} source={Images.profileImage}></Image> : <Image style={{ flex: 1, width: '100%', resizeMode: 'cover' }} source={Images.video_image}></Image>}
        </View>

        <View style={[CommonStyles.ZView.container, { width: '100%', height: '100%', position: 'absolute' }]}>
          <View style={styles.header}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center' }} >
                <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.pop()}>
                  <Image source={Images.back_icon} />
                </TouchableOpacity>
              </View>

              <View style={{ flex: 1.5, justifyContent: 'space-evenly' }} >
                <Text style={{ textAlign: 'center', fontSize: 16, color: this.state.IsPopupShow ? 'white' : 'gray', fontFamily: 'Prompt-SemiBold' }} >Personal Meeting</Text>
                <Text style={{ textAlign: 'center', color: this.state.IsPopupShow ? 'white' : 'gray', fontFamily: 'Prompt-Regular', fontSize: 16 }}>{'1:28:45'}</Text>
              </View>

              <View style={{ flex: 1, alignItems: 'flex-end' }} >
                <View style={{ flex: 1, backgroundColor: '', flexDirection: 'row', justifyContent: 'flex-end', alignContent: 'center', alignItems: 'center' }} >
                  <TouchableOpacity style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}
                    onPress={() => this.props.navigation.navigate('Chat')}>
                    <Image source={Images.chat_icon} />
                  </TouchableOpacity>

                  <TouchableOpacity style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}
                    onPress={this._toggleBottomNavigationView}>
                    <Image source={Images.menu_icon} />
                  </TouchableOpacity>
                </View>
              </View>


            </View>
          </View>


          {/* {this.state.IsPopupShow ? null : (<View style={{ flex: 1, marginTop: 10, maxHeight: 110 }}>
            <FlatList
              horizontal={true}
              contentContainerStyle={{ flexGrow: 1, justifyContent: 'flex-end' }}
              data={DATA}
              keyExtractor={(item, index) => index.toString()}
              renderItem={(item, index) => this._renderItem(item, index)}
            />


          </View>)} */}


          <View style={{ flex: 1, marginTop: 10, maxHeight: 110 }}>
            <FlatList
              horizontal={true}
              contentContainerStyle={{ flexGrow: 1, justifyContent: 'flex-end' }}
              data={DATA}
              keyExtractor={(item, index) => index.toString()}
              renderItem={(item, index) => this._renderItem(item, index)}
            />

          </View>


          <View style={{
            flex: 1,
            marginBottom: 30,
            flexDirection: 'row',
            alignItems: 'flex-end',
            alignContent: 'flex-end',
            justifyContent: 'center',
          }}>

            <View style={{ flex: 1, width: '100%', height: '100%' }}>
              <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
                {this.state.IsPopupShow == true ? (<View style={{ width: 260, marginBottom: 10, height: 100, backgroundColor: 'white', borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
                  <TouchableOpacity style={styles.endButton} onPress={() => { this.props.navigation.pop() }}>
                    <Text style={{ fontFamily: 'Prompt-Regular', fontSize: 12, color: '#FFFFFF' }}>End Meeting For All</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[styles.endButton, { backgroundColor: 'white', top: 5 }]}>
                    <Text style={{ fontFamily: 'Prompt-Regular', fontSize: 12, color: '#131313' }}>Leave Meeting</Text>
                  </TouchableOpacity>
                </View>
                ) : null}
              </View>

              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', maxHeight: 80, flexDirection: 'row' }}>
                <TouchableOpacity style={styles.videoOption}>
                  <Image source={Images.audio_mute_icon} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.videoOption} onPress={() => { this.setState({ IsPopupShow: true }) }}>
                  <Image source={Images.endcall_icon} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.videoOption}>
                  <Image source={Images.video_unmute_icon} />
                </TouchableOpacity>
              </View>
            </View>


          </View>
        </View>

        <CallMenu setModalVisible={this.state.showMenu} ></CallMenu>

        {/* Menu PopUp */}
        <BottomSheet
          visible={this.state.visible}
          onBackButtonPress={this._toggleBottomNavigationView}
          onBackdropPress={this._toggleBottomNavigationView}
        >
          <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{
              borderRadius: 10, backgroundColor: 'white', width: '85%', height: '88%',
              margin: 25
            }}>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.camera_icon} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Select the Sound Device</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.toggle_camera} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Toggle Camera</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.enable_low_bandwidth} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Enable Low Bandwidth Mode</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.camera_icon} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Select the Sound Device</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.camera_icon} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Add Meeting Password</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.start_recording} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Start Recording</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.start_live_stream} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Start Live Stream</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.tile_view} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Enter Tile View</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.camera_icon} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Meeting Info</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.raise_hand} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Raise Your Hand</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.blur} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Blur Background</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.muteall_icon} />
                </View>
                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Muter All</Text>
                </View>
              </View>

            </View>
          </View>
        </BottomSheet>


      </SafeAreaView>
    )
  }
}


export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    textAlign: 'center',
    width: 330,
    height: 50,
    marginBottom: 10,
    lineHeight: 25,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16
  },
  header: {
    flex: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 10,
    maxHeight: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backIcon: {
    width: 40,
    height: 40,
  },
  cameraIcon: {
    right: -20,
    bottom: -20,
    width: 40,
    height: 40,
    position: 'absolute'
  },

  container: {
    flex: 1,
    maxHeight: 55,
    marginLeft: 20,
    marginRight: 20,
    borderBottomWidth: 1.5,
    borderBottomColor: Colors.UNDERLINE,
  },
  textInput: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16
  },
  videoOption: {
    width: 60,
    height: 60,
    marginLeft: 15,
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  endButton: {
    height: 34,
    width: 239,
    backgroundColor: Colors.END_RED,
    alignItems: 'center',
    justifyContent: 'center'
  },
});