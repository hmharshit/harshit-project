import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TextInput, TouchableOpacity, StyleSheet, BackHandler, Platform, Dimensions } from 'react-native';
import { CommonStyles, Colors, Typography, Images } from '../../styles'
import Loader from '../../components/molecules/Loader'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import TopBar from '../../components/atoms/TobBar1'
import ZingTextInput from '../../components/molecules/ZingTextInput'
import { connect } from "react-redux"
import { MeetingCheckUserCount, MeetingGuestAuth } from "../../services/meeting_services"
import { BASE_URL } from "../../services/config"
import { Notification } from "../../components/util"

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

class JoinScreen extends Component {
  constructor(props) {
    super(props);



    this.state = {
      phone: '',
      password: '',
      IsLoader: false,
      buttonDisabled: false
    }
  }

  onStateUpdated(data) {
    const { phone, password } = { data }
    this.setState({ phone: data.phone, password: data.password })
    console.log(this.state);
  }


  componentWillUnmount() {
    if (Platform.OS === "android") {
      this.backHandler.remove()
    }
    this.willFocus.remove()
    this.setState({ meeting_id: null })
  }

  _updateMeetingId() {
    if (this.props.navigation.state.params && this.props.navigation.state.params.meetingId) {
      console.log(this.props.navigation.state.params)
      this.setState({
        meeting_id: this.props.navigation.state.params.meetingId,
        password: this.props.navigation.state.params.meetingPassword,
        personal_link_name: this.props.user_name
      })
    }
  }

  componentDidMount() {

    this.willFocus = this.props.navigation.addListener(
      'willFocus', () => this._updateMeetingId()
    )

    this.setState({personal_link_name: this.props.user_name})


    if (Platform.OS === "android") {
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        console.log("@@@@")
        if (this.props.navigation.state.params && this.props.navigation.state.params.disableProfileIcons) {
          this.props.navigation.pop()
          // this.props.navigation.navigate('Landing')
        }
        else {
          this.props.navigation.pop()
        }
        return true
      })
    }
  }


  checkUserCanJoin = (meeting_id, email) => {

    return MeetingCheckUserCount(meeting_id, email).then(response => {
      if (response && response.error == true && response.status == 500) {
        this.setState({ IsLoader: false })
        Notification(response.result.message, "error")
        return false
      }
      else {
        return true
      }
    }).catch(err => {
      Notification("Meeting Id is not valid!", "error")
      this.setState({ IsLoader: false, buttonDisabled: false })
      console.log(err)
    })

  }

  joinMeeting = async () => {

    const { personal_link_name, meeting_id, password } = this.state
    const { _isUserLoggedIn, user_email, user_image, user_name } = this.props

    let email = user_email || null

    if (!personal_link_name || !meeting_id || !password) {
      // validation
      Notification("All fields are mandatory!", "warning")
      return false
    }

    if (Platform.OS === "android") {
      this.setState({ IsLoader: true })
    }
    this.setState({buttonDisabled: true})

    let user_can_join = await this.checkUserCanJoin(meeting_id, email)
    if (user_can_join) {

      MeetingGuestAuth(personal_link_name, meeting_id, password, email).then(response => {
        this.setState({ IsLoader: false, buttonDisabled: false })

        if (response && !response.error) {
          // correct password
          response = response.result;

          let url = `${response.url}${response.title}?jwt=${response.token}`

          this.props.navigation.navigate('VideoCall', {
            url: url,
            userInfo: {
              // displayName: user_name,
              // avatar: user_image,
              // email: user_email
            }
          })
        }

        else {
          Notification("Wrong password! Please enter the correct password", "error")
          // wrong password
        }
      })
    }

    
    else {
      // Notification("Meeting room is full!", "error")
      // show error
    }
  }


  render() {

    console.log("state", this.state)
    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <TopBar navigation={this.props.navigation}
          renderTopIcons={this.props.navigation.state.params && this.props.navigation.state.params.disableProfileIcons || false}
        />
        <KeyboardAwareScrollView style={{ flex: 1, width: '100%' }} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
          <View style={[CommonStyles.ZView.container, {}]}>
            {/* <TouchableOpacity
                style={{
                  marginLeft: 15,
                  top: 30
                }}
                onPress={() => {
                  if (this.props.navigation.state.params && this.props.navigation.state.params.disableProfileIcons) {
                    this.props.navigation.navigate('Landing')
                  }
                  else {
                    this.props.navigation.navigate('Home')
                  }
                }}
              >
                <Image source={Images.back_icon} />
              </TouchableOpacity> */}
            <View style={[CommonStyles.ZView.centerView, { justifyContent: 'center', flex: 1 }]}>

              <View style={{ width: DeviceWidth, height: DeviceHeight / 100 * 10, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ textAlign: 'center', color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 15 }}>{"To Join a meeting, Please fill the details \n below"}</Text>
              </View>
              <ZingTextInput extraStyle={{ marginTop: 20, marginBottom: 15 }} text={this.state.personal_link_name} phoneOptions={false} secureTextEntry={false} placeholder={"Name"} onChangeText={(text) => { this.setState({ personal_link_name: text }) }} ></ZingTextInput>
              <ZingTextInput extraStyle={{ marginBottom: 15, color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 15 }} text={this.state.meeting_id} phoneOptions={false} secureTextEntry={false} placeholder={"Meeting Id"} onChangeText={(text) => { this.setState({ meeting_id: text }) }}></ZingTextInput>
              <ZingTextInput extraStyle={{ marginBottom: 15, color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 15 }}  text={this.state.password} phoneOptions={false} secureTextEntry={true} placeholder={"Password"} onChangeText={(text) => { this.setState({ password: text }) }} ></ZingTextInput>
              <TouchableOpacity disabled={this.state.buttonDisabled} style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView, { marginTop: 80 }]} onPress={this.joinMeeting}>
                <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Join Meeting</Text>
              </TouchableOpacity>
            </View>

          </View>
        </KeyboardAwareScrollView>




        <Loader setModalVisible={this.state.IsLoader} ></Loader>

      </SafeAreaView>
    )
  }
}

const _mapStateToProps = (state) => {

  return {
    _isUserLoggedIn: state['home'].is_user_loggedin || false,
    user_email: state['home'].user_data && state['home'].user_data.email,
    user_name: state['home'].user_data && state['home'].user_data.name,
    user_image: state['home'].user_data && `${BASE_URL}/uploads/userimage/${state['home'].user_data.profile_img}`

  }
}

export default connect(_mapStateToProps)(JoinScreen)

export const joinStyles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    textAlign: 'center',
    width: 330,
    height: 100,
    marginBottom: 10,
    lineHeight: 25,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16
  },
});