import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity, AsyncStorage, Linking, Platform, Dimensions } from 'react-native';
import { Images, CommonStyles } from '../../styles'
import { setIsUserLoggedIn, setUserData, setUserNavigatedFromUrl } from "../../actions/home"
import base64 from 'react-native-base64'
import { connect } from "react-redux"


class LandingScreen extends Component {
  constructor(props) {
    super(props);
  }


  _handleIncomingUrl = (url, state) => {

    console.log("Incoming URL", url, state)

    if (!url) return false

    let meetingId, meetingPassword;
    url = decodeURIComponent(url)
    meetingId = url.split('?')[0].split('/').slice(-1).pop()
    meetingPassword = url.split('?')[1].substr(9)

    meetingId = base64.decode(meetingId)
    meetingPassword = base64.decode(meetingPassword)

    this.props.dispatch(setUserNavigatedFromUrl(true))

    
    if (Platform.OS === "ios") {
      setTimeout(() => {
        this.props.navigation.navigate('Join', {
          disableProfileIcons: true,
          meetingId: meetingId,
          meetingPassword: meetingPassword
        })
      }, state == "initial" ? 100 : 1500)
    }
    else {
      setTimeout(() => {
        this.props.navigation.navigate('Join', {
          disableProfileIcons: true,
          meetingId: meetingId,
          meetingPassword: meetingPassword
        })
      }, state == "initial" ? 100 : 1500)
    }
  }


  componentWillUnmount() {
    Linking.removeEventListener('url', this._handleIncomingUrl)
  }


  componentDidMount() {

    Linking.addEventListener('url', ({ url }) => {
      this._handleIncomingUrl(url, "initial")
    })

    Linking.getInitialURL().then(url => {
      if (!this.props._isUserNavigated)
        this._handleIncomingUrl(url, "background")
    }
    )


    AsyncStorage.getItem("user").then(user => {
      console.log("User", user)
      if (user && user.length > 0) {
        console.log("YEs")
        user = JSON.parse(user)
        console.log(user)
        this.props.dispatch(setIsUserLoggedIn(true))
        this.props.dispatch(setUserData(user))
        console.log("*********************", "naivigating to home")
        this.props.navigation.navigate('Home')
      }
    })
  }

  render() {
    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <View style={CommonStyles.ZView.container}>
          <View style={[CommonStyles.ZView.container1, CommonStyles.ZView.logoView]}>
            <Image source={Images.logo} />
          </View>
          <View style={[CommonStyles.ZView.centerView1, CommonStyles.ZView.centerImageView]}>
            <Image source={Images.mobImage} style={{ bottom: 20 }} />
          </View>
          <View style={[CommonStyles.ZView.container, {  bottom: 35 }]}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
              <TouchableOpacity style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView]} onPress={() => {
                this.props.navigation.navigate('Join', {
                  disableProfileIcons: true
                })
              }}>
                <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Join Meeting</Text>
              </TouchableOpacity>
            </View>

            <View style={{ flex: 1, margin: 20, paddingTop:20, flexDirection: 'row', justifyContent: 'center' }} >
              <TouchableOpacity style={[CommonStyles.ZButton.halfgreen, CommonStyles.ZView.centerImageView]} onPress={() => { 
                this.props.navigation.navigate('SignUp') 
                }}>
                <Text style={{ color: '#8BC341', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Sign Up</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[CommonStyles.ZButton.halfgreen, CommonStyles.ZView.centerImageView]} onPress={() => { this.props.navigation.navigate('Login') }}>
                <Text style={{ color: '#8BC341', fontFamily: 'Prompt-Medium', fontSize: 15 }} >Sign In</Text>
              </TouchableOpacity>
            </View>

          </View>
        </View>
      </SafeAreaView>


    );
  }
}

const _mapStateToProps = (state) => {
  return {
    _isUserNavigated: state['home'].is_user_navigated
  }
}




export default connect(_mapStateToProps)(LandingScreen);

