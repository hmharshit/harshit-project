import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity, ScrollView, StyleSheet, FlatList, Platform, BackHandler } from 'react-native';
import { Images, CommonStyles, Colors } from '../../styles'

import TopBar from '../../components/atoms/TopBar'
import Loader from '../../components/molecules/Loader'
import * as RNLocalize from "react-native-localize";
import { FetchUpcomingMeetings, MeetingJwtAuth, MeetingInvitation, TimezoneMeetingStart } from "../../services/meeting_services"
import { connect } from "react-redux"
import moment from "moment";
import { BASE_URL } from "../../services/config"
import { Share } from 'react-native';
import { MONTH_MAPPING, R_MONTH_MAPPING, MEETING_LINK_INVITE_FORMAT } from "../../services/constants";
import { PRIMARY } from '../../styles/colors';

String.prototype.formatUnicorn =
  String.prototype.formatUnicorn ||
  function () {
    let str = this.toString();
    if (arguments.length) {
      const t = typeof arguments[0];
      let key;
      const args =
        t === 'string' || t === 'number'
          ? Array.prototype.slice.call(arguments)
          : arguments[0];

      for (key in args) {
        str = str.replace(new RegExp(`\\{${key}\\}`, 'gi'), args[key]);
      }
    }
    return str;
};


class HomeScreen extends Component {
  constructor(props) {
    super(props)
    console.disableYellowBox = true

    this.state = {
      upcoming_meetings: {},
      IsLoader: false,
      uniquePaymentLink: 'https://zingallo.com',
    }
  }


  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress')
    this.setState({})
  }

  shareMultipleImages = async (link, data) => {


    let args = {
      "name": this.props.user_name,
      "time_zone": data.timezone,
      "meeting_time": data.start_time,
      "meeting_id": data.rand_id,
      "password": data.password,
      "meeting_topic": data.title,
      "meeting_url": link
    }

    let text = MEETING_LINK_INVITE_FORMAT.formatUnicorn(args)


    // If you want, you can use a try catch, to parse
    // the share response. If the user cancels, etc.

    Share.share(
      {
        message: text,
        title: "Share Link"
      },
      {
        dialogTitle: "Share Link", // Android
        subject: "Share Link" // iOS
      }).then(success => console.log(success), reason => console.log(reason))
  }


  startMeeting = (meeting_id) => {

    this.setState({ IsLoader: true })


    const { user_name, user_image, user_email } = this.props

    return MeetingJwtAuth(this.props.user_id, meeting_id).then(response => {
      this.setState({ IsLoader: false })


      if (response && !response.error && response.status === 200) {

        console.log(response.result.url)

        this.props.navigation.navigate('VideoCall', {
          url: response.result.url,
          userInfo: {
            displayName: user_name
          }
        })
      }
    })
  }


  startMeetingwithTimezone = async () => {

    this.setState({ IsLoader: true })


    const { user_name } = this.props

    return TimezoneMeetingStart(this.props.user_id, RNLocalize.getTimeZone()).then(response => {
      this.setState({ IsLoader: false })


      if (response && !response.error && response.status === 200) {

        console.log(response.result.url)

        this.props.navigation.navigate('VideoCall', {
          url: response.result.url,
          userInfo: {
            displayName: user_name
          }
        })
      }
    })
  }


  renderItem = (item) => {
    return (
      <View style={{
        flex: 1, marginBottom: 10, marginTop: 10, height: 90,
        borderLeftColor: Colors.PRIMARY, borderLeftWidth: 5
      }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, marginLeft: 10, justifyContent: 'space-evenly' }} >
            <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 15 }}>{item.item.title}</Text>
            <Text style={{ color: '#8BC341', fontFamily: 'Prompt-Regular', fontSize: 12 }}>{item.item.date}</Text>
          </View>
          <View style={{ flex: 1, maxWidth: 220, flexDirection: 'row' }} >
            <View style={{ flex: 0.25, flexDirection: 'column', margin: 5, alignItems: 'center', justifyContent: 'center', }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('EditSchedule', {
                  meet_id: item.item.id,
                  title: item.item.title,
                  description: item.item.description,
                  end_hour: item.item.end_hour,
                  end_minute: item.item.end_min,
                  start_time: item.item.start_time,
                  meeting_status: item.item.meeting_status,
                  timezone: item.item.timezone,
                  meet_rand_id: item.item.rand_id,
                  meeting_password: item.item.password,
                })}
                style={[CommonStyles.ZButton.fullgreen, { width: 35, maxHeight: 30, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }]}>
                <Image source={Images.edit_image} />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 0.25, flexDirection: 'column', margin: 5, alignItems: 'center', justifyContent: 'center', }}>
              <TouchableOpacity
                // onPress={() => this.startMeeting(item.item.id)}
                onPress={() => this.GetInvitationLink(item.item.id, item.item)}
                // onPress={() => this.GetInvitationLink(item.item)}
                style={[CommonStyles.ZButton.fullgreen, { width: 35, maxHeight: 30, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }]}>
                <Image source={Images.share_image} />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 0.50, flexDirection: 'column', margin: 5, alignItems: 'center', justifyContent: 'center', }}>
              <TouchableOpacity
                onPress={() => this.startMeeting(item.item.id)}
                style={[CommonStyles.ZButton.fullgreen, { width: 70, maxHeight: 30, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }]}>
                <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 12 }}>Start</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View >
    )
  }
  GetInvitationLink = (id, data) => {

    if (!id) {
      return false;
    }

    if (Platform.OS === "android") {
      this.setState({
        IsLoader: true,
      });
    }

    MeetingInvitation(id, this.props.user_id).then((response) => {
      if (response && !response.error && response.status === 200) {
        this.setState({ uniquePaymentLink: response.result.meeitngUrl });
        this.shareMultipleImages(response.result.meeitngUrl, data)
      } else {
        Notification(response.result.message, "error", 0);
      }
      this.setState({ IsLoader: false });
    }).catch(err => {
      Notification(`${err}`, "error")
      this.setState({ IsLoader: false });
      console.log(err)
    });
  }

  // makeClipboardText = (data) => {
  //   const { uniquePaymentLink } = this.state

  //   let args = {
  //     "name": this.props.user_name,
  //     "time_zone": data.timezone,
  //     "meeting_time": data.date,
  //     "meeting_id": data.rand_id,
  //     "password": data.password,
  //     "meeting_topic": data.title,
  //     "meeting_url": uniquePaymentLink
  //   }

  //   let text = MEETING_LINK_INVITE_FORMAT.formatUnicorn(args)
  //   // alert(text)
  //   if (text !== null && text !== '') {
  //     this.shareLink(text)
  //   } else {
  //     return true;
  //   }
  // };

  renderUpcomingMeetings() {
    let { upcoming_meetings } = this.state
    let dates = Object.keys(upcoming_meetings);

    dates = dates.reverse()

    return dates.map(date => {
      let data = []
      let temp = upcoming_meetings[date]
      temp.map(t => {
        data.push({
          id: t.id,
          rand_id: t.meeting_rand_id,
          date: moment(t.showDatetime, ["YYYY-MM-DD hh:mm:ss A"]).format('dddd hh:mm A'),
          title: t.title,
          description: t.description,
          start_time: t.start_time,
          end_hour: t.end_hour,
          end_min: t.end_min,
          meeting_status: t.Status,
          timezone: t.timezone,
          password: t.password
        })
      })
      return (
        <View>

          <View style={[styles.botomTitleView, { maxHeight: 30, marginTop: 10, marginBottom: 10 }]}>
            <Text style={styles.dayText} >{moment(date).format('dddd MMMM, Do YYYY')}</Text>
          </View>

          <ScrollView style={{ maxHeight: 200 }} nestedScrollEnabled={true}>
            <FlatList
              style={{ flex: 1, marginLeft: 20, marginRight: 20 }}
              data={data}
              renderItem={this.renderItem}
              keyExtractor={item => item.id} />
          </ScrollView>
        </View>
      )
    })
  }



  fetchUpcomingMeetings() {
    let upcoming_meetings = []

    let dateTime = moment().format('YYYY-MM-DD HH:mm:ss')


    if (Platform.OS === "android") {
      this.setState({
        IsLoader: true
      })
    }

    if (Platform.OS === "ios") {
      this.setState({
        IsLoader: false
      })
    }

    return FetchUpcomingMeetings(this.props.user_id, dateTime, RNLocalize.getTimeZone())
      .then(response => {

        let is_zero_meetings = false

        this.setState({ IsLoader: false })

        if (response && !response.error && response.status == 200) {

          let meetings = response.result.upcomingmeeting

          if (meetings && Array.isArray(meetings) && meetings.length === 0) {
            // zero meetings
            upcoming_meetings = []
            is_zero_meetings = true
          }

          if (meetings && typeof meetings === "object") {
            upcoming_meetings = meetings
          }

        }

        this.setState({ IsLoader: false, upcoming_meetings: upcoming_meetings, is_zero_meetings: is_zero_meetings })
      }).catch(err => {
        this.setState({ IsLoader: false })
        console.log("Errr", err)
      })

  }


  componentWillUnmount() {
    this.willFocus.remove()
  }

  componentWillUpdate() {
    BackHandler.addEventListener('hardwareBackPress', () => BackHandler.exitApp())
  }


  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => BackHandler.exitApp())

    this.fetchUpcomingMeetings()

    this.willFocus = this.props.navigation.addListener(
      'willFocus', () => {
        this.fetchUpcomingMeetings()
      }
    )
  }

  render() {

    return (
      <SafeAreaView style={styles.mainContainer}>
        <TopBar navigation={this.props.navigation}></TopBar>
        <View style={styles.subContainer}>
          <View style={styles.contentView} >

            <TouchableOpacity onPress={this.startMeetingwithTimezone} style={styles.joinbutton}>
              <Image source={Images.video_call_image} />
              <Text style={styles.joinbuttontext}>Start a Meeting</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.joinbutton, { marginTop: 15 }]}
              onPress={() => this.props.navigation.navigate('Join')}>
              <Image source={Images.video_icon} />
              <Text style={styles.joinbuttontext}>Join a Meeting</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.joinbutton, { marginTop: 15 }]}
              onPress={() => this.props.navigation.navigate('Schedule')} >
              <Image source={Images.calendar_icon} />
              <Text style={styles.joinbuttontext} >Schedule a Meeting</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.bottomview} >
          {this.state.is_zero_meetings ? <Text style={styles.noMeetingLabel} >No Meetings Scheduled</Text> :

            <ScrollView nestedScrollEnabled={true} style={{ marginTop: 20 }}>
              {this.renderUpcomingMeetings()}
            </ScrollView>}

          </View>
        </View>
        <Loader setModalVisible={this.state.IsLoader}></Loader>
      </SafeAreaView>
    )
  }
}

const _mapStateToProps = (state) => {

  return {
    user_id: state['home'].user_data && state['home'].user_data.id,
    user_name: state['home'].user_data && state['home'].user_data.name,
    user_image: state['home'].user_data && `${BASE_URL}/uploads/userimage/${state['home'].user_data.profile_img}`,
    user_email: state['home'].user_data && state['home'].user_data.email
  }
}

export default connect(_mapStateToProps)(HomeScreen)
const styles = StyleSheet.create({
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    flex: 1,
    marginTop: 20,
    flexDirection: 'column'
  },
  contentView: {
    flex: 1,
    maxHeight: 280,
    justifyContent: 'center'
  },
  joinbutton: {
    flex: 1,
    marginTop: 10,
    maxHeight: 83,
    marginLeft: 20,
    marginRight: 20,
    borderColor: Colors.PRIMARY,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  joinbuttontext: {
    top: 5,
    color: '#8BC341',
    fontSize: 15,
    fontFamily: 'Prompt-SemiBold'
  },

  bottomview: {
    flex: 1,
    marginTop: 20,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: { width: 0, height: -7 },
    shadowOpacity: 0.09,
    shadowRadius: 9.51,
    elevation: 15,
    borderTopStartRadius: 40,
    borderTopEndRadius: 40
  },
  botomTitleView: {
    flex: 1,
    maxHeight: 50,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'space-evenly',
    fontSize: 15,
    fontFamily: 'Prompt-SemiBold',
  },
  dayText: {
    alignContent: 'center',
    fontSize: 15,
    fontFamily: 'Prompt-SemiBold',
    color: '#131313'
  },
  meetingLabel: {
    color: Colors.PRIMARY,
    alignContent: 'center',
    fontSize: 15,
    fontFamily: 'Prompt-Regular',
    color: '#8BC341'
  },
  noMeetingLabel: {
    flex: 1,
    color: Colors.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 16,
    top: 100,
    alignSelf: 'center',
    fontFamily: 'Prompt-Regular',
    color: '#8BC341',
    color:Colors.GRAY_LIGHT
  },
});
