import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TextInput, Dimensions,
  TouchableOpacity,
  Platform
} from "react-native";
import { Images, CommonStyles, Colors } from "../../styles";
import SigninCard from "../../components/organisms/SigninCard";
import Loader from "../../components/molecules/Loader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import GoogleSignInIcon from "../../components/organisms/GoogleSignIn";
import AppleSignIn from "../../components/organisms/AppleSignIn";

import { connect } from "react-redux"
import { SendOtp } from "../../services/auth_services"
import { setRegistrationMobile, setCountryCode } from "../../actions/register"
import { Notification } from "../../components/util"

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

class SignupScreen extends Component {
  constructor(props) {
    super(props);
    // console.disableYellowBox = true;

    this.state = {
      phone: "",
      password: "",
      IsLoader: false,
      country_code: "91"
    };

    this.onStateUpdated = this.onStateUpdated.bind(this)
  }

  onStateUpdated(data) {
    // console.log("d", data)
    const { phone, password } = { data };
    this.setState({ phone: data.phone, password: data.password });
    // console.log(this.state);
  }


  checkValidNumber(number) {
    if (number.indexOf(".") == -1 && number.length == 10) return true
    else return false
  }

  isSupported = () => {
    const majorVersionIOS = parseInt(Platform.Version, 10)
    return majorVersionIOS >=13 && Platform.OS === "ios" ? true : false
  }

  sendOtp = () => {

    if (!this.state.phone) {
      Notification("Please enter the mobile number!", "warning", 0)
      return false
    }

    if (!this.checkValidNumber(this.state.phone)) {
      Notification("Please enter a valid number!", "warning", 0)
      return false
    }
    this.setState({ IsLoader: true });
    console.log("here", this.state)
    SendOtp(this.state.phone, this.state.country_code).then(response => {
      console.log("Response --> ", response)
      this.setState({ IsLoader: false });

      if (response && response.error == false) {
        let result = response.result;

        if (result && result.message && result.message == "OTP send successfully") {
          Notification("OTP sent successfully!", "success", 0)
          this.setState({ otp: result.otp.toString(), phone_number_attached: this.state.phone })
        }
      }

      else {
        Notification(response.result.message, "warning", 0)
      }




    }).catch(err => {
      this.setState({ IsLoader: false });
      console.log(err)})
  }

  checkOtpExpiration = () => {
    // consoe
  }

  verifyOtp = () => {

    if (!this.state.phone) {
      Notification("Please enter the mobile number!", "warning", 0)
      return false
    }


    if (!this.checkValidNumber(this.state.phone)) {
      Notification("Please enter a valid number!", "warning", 0)
      return false
    }

    if (this.state.phone != this.state.phone_number_attached) {
      Notification("This OTP is not valid for this number.", "warning", 0)
      return false
    }


    let original_otp = this.state.otp;
    let user_entered_otp = this.state.password;

    if (original_otp === user_entered_otp) {
      this.props.dispatch(setRegistrationMobile(this.state.phone))
      this.props.dispatch(setCountryCode(this.state.country_code))
      Notification("OTP verified successfully!", "success", 0)
      this.props.navigation.navigate("Password", { 'mobile': this.state.phone })
    }
    else {
      Notification("Wrong OTP!", "error", 0)
    }
  }


  render() {
    const { otp_field_enabled } = this.props
    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <KeyboardAwareScrollView
          style={{ flex: 1, width: "100%" }}
          contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
        >
          <View style={CommonStyles.ZView.container}>
            <View
              style={[
                CommonStyles.ZView.container,
                CommonStyles.ZView.logoNewView,
              ]}
            >
              <TouchableOpacity style={{ marginLeft: 20, marginRight: 10, marginTop: 20 }}
                onPress={() => this.props.navigation.pop()}
              >
                <Image source={Images.back_image} />
              </TouchableOpacity>

              <Image source={Images.logo} style={{
                justifyContent: 'center', alignItems: 'center', position: 'absolute',
                left: DeviceWidth / 100 * 26, top: DeviceHeight / 100 * 9.3
              }} />
            </View>


            <View style={[CommonStyles.ZView.centerView2]}>

              <SigninCard
                country_code_handler={(cc) => this.setState({ country_code: cc })}
                showVerify={true}
                resendOtp={() => this.sendOtp()}
                verifyButtonHandler={() => this.sendOtp()}
                otpFieldEnabled={otp_field_enabled}
                cardType={"signup"}
                dataChanged={(data) => {
                  console.log("dada")
                  console.log(data)
                  this.onStateUpdated(data);
                }}
              />
            </View>

            <View
              style={[
                CommonStyles.ZView.container,
                { justifyContent: "space-evenly", top: -30 },
              ]}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                }}
              >
                <TouchableOpacity
                  style={[
                    CommonStyles.ZButton.fullgreen,
                    CommonStyles.ZView.centerImageView,
                  ]}
                  onPress={() => {
                    this.verifyOtp()
                  }}
                >
                  <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Sign Up</Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  marginBottom:10
                }}
              >
                <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 12 }}>
                  Or Sign In with
                </Text>
              </View>
              {this.isSupported() && <AppleSignIn
                  navigation={this.props.navigation}
                  text={"Sign Up with Apple"}
              />}
              <GoogleSignInIcon
                text={"Sign Up with Google"}
                style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}
                navigation={this.props.navigation}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Loader setModalVisible={this.state.IsLoader}></Loader>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log(state)
  return {
    enable_otp_field: state["register"].otp_field_enabled
  }
}
export default connect(mapStateToProps)(SignupScreen);
