export const PRIMARY = '#8BC341';
export const RED = '#D74937';
export const END_RED = '#FF0606';
export const BLUE = '#1492E6';
export const GRAY = '#E9E9E9';




export const SECONDARY = '#767676';
export const WHITE = '#FFFFFF';
export const BLACK = '#000000';
export const UNDERLINE = '#E5E5E5';


// ACTIONS
export const SUCCESS = '#3adb76';
export const WARNING = '#ffae00';
export const ALERT = '#cc4b37';

// GRAYSCALE
export const GRAY_LIGHT = '#e6e6e6';
export const GRAY_MEDIUM = '#cacaca';
export const GRAY_DARK = '#8a8a8a';


export const TEXT_PLACEHOLDER = '#999999';
