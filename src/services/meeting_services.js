
import API_ENDPOINTS from "./config"

export const FetchUpcomingMeetings = async (user_id, datetime, timezone) => {
    /**
     * It fetches the upcoming meetings
     */
    const payload = {
        "user_id": user_id,
        "datetime": datetime,
        "timezone": timezone
    }

    console.log(payload)

    let meeting_list = await fetch(API_ENDPOINTS.fetch_meetings.endpoint, {
        method: API_ENDPOINTS.fetch_meetings.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await meeting_list.json()
    return response
}



export const MeetingJwtAuth = async (user_id, meeting_id) => {
    /**
     * API to fetc 
     */
    const payload = {
        "user_id": user_id,
        "meeting_id": meeting_id
    }

    console.log(payload)

    let meeting_list = await fetch(API_ENDPOINTS.meeting_jwt_auth.endpoint, {
        method: API_ENDPOINTS.meeting_jwt_auth.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await meeting_list.json()
    return response
}

export const TimezoneMeetingStart = async (user_id, timezone) => {
    /**
     * API to fetch
     */
    const payload = {
        "user_id": user_id,
        "timezone": timezone
    }

    console.log(payload)

    let meeting_start = await fetch(API_ENDPOINTS.start_meeting_timezone.endpoint, {
        method: API_ENDPOINTS.start_meeting_timezone.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await meeting_start.json()
    return response
}


export const MeetingGuestAuth = async (name, meeting_id, password, email) => {
    const payload = {
        name: name,
        meeting_id: meeting_id,
        password: password
    }

    if (email) payload.email = email

    console.log(payload)

    let meeting_list = await fetch(API_ENDPOINTS.meeting_guest_auth.endpoint, {
        method: API_ENDPOINTS.meeting_guest_auth.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await meeting_list.json()
    return response
}


export const MeetingCheckUserCount = async (meeting_id, email) => {
    let payload = {
        "meeting_id": meeting_id
    }

    if (email) payload.email = email

    console.log(payload)

    let check_user_count = await fetch(API_ENDPOINTS.meeting_check_count.endpoint, {
        method: API_ENDPOINTS.meeting_check_count.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await check_user_count.json()
    return response
}


export const CheckSubscription = async (user_id) => {
    let payload = {
        user_id
    }

    console.log(payload)

    let user_subscription = await fetch(API_ENDPOINTS.check_subscription_plan.endpoint, {
        method: API_ENDPOINTS.check_subscription_plan.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await user_subscription.json()
    console.log(response)

    return response
}



export const ScheduleNewMeeting = async (title, timezone, date,
    start_time, end_hours, end_minute, meeting_password, password, host_video_on,
    participant_video_on, add_to_calendar, user_id, description) => {
    const payload = {
        "title": title,
        "timezone": timezone,
        "date": date,
        "start_time": start_time,
        "end_hours": end_hours,
        "end_minuts": end_minute,
        "meeting_password": meeting_password,
        "password": password,
        "host_video_on": host_video_on,
        "participant_video_on": participant_video_on,
        "add_to_calendar": add_to_calendar,
        "user_id": user_id,
        "description": description
    }

    console.log(payload)

    let create_meeting = await fetch(API_ENDPOINTS.schedule_meeting.endpoint, {
        method: API_ENDPOINTS.schedule_meeting.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await create_meeting.json()
    console.log(response)

    return response
}


// Update Schedule Meeting
export const UpdateScheduleMeeting = async (title, timezone, date,
    start_time, end_hours, end_minute, meeting_password, password, host_video_on,
    participant_video_on, add_to_calendar, user_id, status, id, description) => {
    const payload = {
        "title": title,
        "timezone": timezone,
        "date": date,
        "start_time": start_time,
        "end_hours": end_hours,
        "end_minuts": end_minute,
        "meeting_password": meeting_password,
        "password": password,
        "host_video_on": host_video_on,
        "participant_video_on": participant_video_on,
        "add_to_calendar": add_to_calendar,
        "user_id": user_id,
        "status": status,
        "id": id,
        "description": description
    }

    console.log(payload)

    let update_meeting = await fetch(API_ENDPOINTS.update_schedule_meeting.endpoint, {
        method: API_ENDPOINTS.update_schedule_meeting.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await update_meeting.json()
    console.log(response)

    return response
}

// Meeting Invitation
export const MeetingInvitation = async (id, user_id) => {
    const payload = {
        "id": id,
        "user_id": user_id,
    }

    console.log(payload)

    let meeting_invite = await fetch(API_ENDPOINTS.meeting_invitation.endpoint, {
        method: API_ENDPOINTS.meeting_invitation.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await meeting_invite.json()
    return response
}

// Delete Meeting
export const DeleteMeeting = async (id) => {
    const payload = {
        "id": id
    }

    console.log(payload)

    let meeting_delete = await fetch(API_ENDPOINTS.delete_meeting.endpoint, {
        method: API_ENDPOINTS.delete_meeting.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await meeting_delete.json()
    return response
}