import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import AuthNavigator from './auth-navigator';
import AppNavigator from './app-navigator';
import HomeNavigator from './home-navigator'


const RootNavigator = createSwitchNavigator(
  {
    Auth: AuthNavigator,
    App: AppNavigator,
    Main: HomeNavigator
  },
  {
    initialRouteName: 'Auth',
  },
);

export default createAppContainer(RootNavigator);
