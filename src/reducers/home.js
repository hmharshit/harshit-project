import {
    SET_IS_USER_LOGGED_IN, 
    SET_USER_META_DATA,
    SET_USER_NAVIGATED_FROM_INITIAL_URL
} from "../actions/types"


const initialState = {
    user_data: {},
    is_user_loggedin: false,
    is_user_navigated: false
}

const homeReducer = (state=initialState, action) => {
    switch(action.type) {
        case SET_USER_META_DATA:
            return {
                ...state,
                user_data: action.user_data
            }
        case SET_IS_USER_LOGGED_IN:
            return {
                ...state,
                is_user_loggedin: action.user_logged_in
            }

        case SET_USER_NAVIGATED_FROM_INITIAL_URL: 
            return {
                ...state,
                is_user_navigated: action.navigated
            }
        default:
            return state;
    }
}

export default homeReducer;